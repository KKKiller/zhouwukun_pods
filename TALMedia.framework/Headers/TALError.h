#import <Foundation/Foundation.h>

// Constant to identify the TALMedia error domain.
FOUNDATION_EXPORT NSString* const TALMediaErrorDomain;

// Keys in the user info dictionary in errors TALMedia creates.
FOUNDATION_EXPORT NSString* const TALMediaErrorReasonKey;

// Codes that specify an error. These may appear in NSError objects returned by various TALMedia methods.
typedef NS_ENUM(NSInteger, TALMediaError) {
    // Generic error code
    TALMediaErrorUnknown = -1,
    TALMediaErrorNoError = 0,
    TALMediaErrorInternalError,
    
    // TALPlayer error code
    TALMediaErrorConnectStreamFailed,
    TALMediaErrorBadStream,
    TALMediaErrorEndOfStream,
    
    // TALPublisher error code
    TALMediaErrorPublisherNetworkError, // automatically reconnect handled by the framework
    TALMediaErrorPublisherNetworkResume,
    TALMediaErrorResolutionInvalid,
    TALMediaErrorVideoCodecInitError,
    TALMediaErrorCameraDeviceIDInvalid,
    TALMediaErrorMicrophoneDeviceIDInvalid,
    TALMediaErrorSpeakerDeviceIDInvalid,
    TALMediaErrorScreenParameterInvalid,
    TALMediaErrorOpenCameraFailed,
    TALMediaErrorOpenMicrophoneFailed,
    TALMediaErrorOpenRecordFileFailed,
    TALMediaErrorDiskFull,
};
